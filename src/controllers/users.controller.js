const User = require('../models/user.model');

const userController = {};

/**
 * @description Register user
 * @route POST api/users
 * @access Public
 * @param {Object} req
 */
userController.createUser = async (req, res) => {
    // Inicializamos el try catch
    try {
        // Validamos que exista datos en el body
        if (!req.body) {
            return res.status(400).json({
                success: false,
                message: 'No se ha enviado ningun dato'
            });
        }

        // Validamos que no exista otro usuario con la misma identificacion
        const userId = await User.findOne({ identification: req.body.identification });
        // Si existe un usuario con la misma identificación devolvemos el error
        if (userId) {
            // Devolvemos el error
            return res.status(400).json({
                success: false,
                message: 'La identificacion ya existe'
            });
        }

        // Asignamos el nuevo correo al usuario
        req.body.email = await userController.generateEmail(req.body.first_name.toLowerCase(), req.body.paternal_surname.toLowerCase().replaceAll(' ', ''));;
        // Creamos el nuevo usuario
        const userCreated = await User.create(req.body);
        // Retornamos el usuario creado
        return res.status(200).json({
            success: true,
            message: 'Usuario creado',
            user: userCreated
        });
    } catch (error) {
        // Capturamos y enviamos el error
        return res.status(500).json({
            success: false,
            message: 'Error al crear el usuario',
            error
        });
    }
}

/**
 * @description Obtener usuarios paginados
 * @route GET /users/all
 * @access Public 
 * @param {Object} req.params
 */
userController.getUsers = async (req, res) => {
    // Inicializamos el try catch
    try {
        // Contamos los usuarios en Base de Datos
        const countData = await User.count();
        // Obtenemos el número total de paginas segun la cantidad de datos
        const numPages = Math.ceil(countData / 10);
        // Obtenemos la pagina que quiere ver el usuario
        const currentPage = (parseInt(req.query.page) - 1) * 10;
        // Obtenemos los usuarios paginados con un limite de 10
        const users = await User.find().sort({ createdAt: -1 }).skip(currentPage).limit(10);
        // Retornamos el usuario creado
        return res.status(200).json({
            success: true,
            message: 'Usuarios',
            users,
            numPages,
            totalCount: countData
        });
    } catch (error) {
        console.log(error);
        // Capturamos y enviamos el error
        return res.status(500).json({
            success: false,
            message: 'Error al obtener usuarios',
            error
        });
    }
}

/**
 * @description Editar un usuario
 * @route GET /users/edit/:id
 * @access Public 
 * @param {Object} req.params req.body
 */
userController.updateUser = async (req, res) => {
    // Inicializamos el try catch
    try {
        // Verificamos que la identifiación este registrada en base de datos
        const userId = User.findOne({ identification: req.body.identification });
        // Validamos si existe un usuario
        if (!userId) {
            // Si no existe usuario enviamos la repuesta
            return res.status(400).json({
                success: false,
                message: 'Identificación no registrada en nuestro sistema'
            })
        }

        // Verificamos que el email este registrado en base de datos
        const userEmail = User.findOne({ email: req.body.email });
        // Validamos si existe un usuario
        if (!userEmail) {
            // Si no existe usuario enviamos la repuesta
            return res.status(400).json({
                success: false,
                message: 'Email no registrado en nuestro sistema'
            })
        }

        // Si existen el email y la identificación en base de datos
        // Ahora validamos que el primer nombre o el primer apellido hayan cambiado
        const emailArr = req.body.email.split('.');
        // Validamos el primer nombre
        if (req.body.first_name.toLowerCase() !== emailArr[0]) {
            // Creamos el nuevo email
            req.body.email = await userController.generateEmail(req.body.first_name.toLowerCase(), req.body.paternal_surname.toLowerCase().replaceAll(' ', ''));
        }
        // Ahora validamos el primer apellido
        if (req.body.paternal_surname.toLowerCase().trim() !== emailArr[2]) {
            // Creamos el nuevo email
            req.body.email = await userController.generateEmail(req.body.first_name.toLowerCase(), req.body.paternal_surname.toLowerCase().replaceAll(' ', ''));
        }

        // Ahora que ya validamos si el primer nombre o el primer apellido hayan cambiado
        // Actualizamos el usuario
        await User.findByIdAndUpdate(req.params.id, req.body, { new: true })
        // Enviamos la respuesta y el usuario actualizado
        return res.status(200).json({
            success: true,
            message: 'Usuario ha sido actualizado'
        })

    } catch (error) {
        console.log(error);
        // Obtenemos el error y enviamos una respuesta
        res.status(500).json({
            success: false,
            message: 'Error al editar el usuario',
            error
        });
    }
}

/**
 * @description Filtrar usuarios
 * @route GET /users/filter
 * @access Public 
 * @param {Object} req.params
 */
userController.getUsersFilter = async (req, res) => {
    // Inicializamos el try catch
    try {
        // Obtenemos el dato y la propiedad por la que se quiere filtrar
        const filtro = req.query.data;
        const propiedad = req.query.filter;
        // Verificamos que el dato y la propiedad no esten vacios
        if (filtro && propiedad) {
            // Obtenemos los usuarios filtrados
            const users = await User.find({ [propiedad]: filtro });
            // Retornamos los usuarios filtrados
            return res.status(200).json({
                success: true,
                message: 'Usuarios filtrados',
                users
            })
        } else {
            // Si no hay datos retornamos una respuesta
            return res.status(400).json({
                success: false,
                message: 'Filtro o propiedad vacios'
            })
        }

    } catch(error) {
        // Obtenemos el error
        console.log(error);
        return res.status(500).json({
            success: false,
            message: 'Error al filtrar usuarios',
            error
        })
    }
}

userController.generateEmail = async (firstName, paternalSurname) => {
    // Creamos el correo del nuevo usuario
    let newEmail = `${firstName.toLowerCase()}.${paternalSurname.toLowerCase().replaceAll(' ', '')}@cidenet.com`;
    // Validamos que no exista otro email con el mismo correo
    const user = await User.findOne({ email: newEmail });
    // Si existe el mismo correo, le creamos un identificador al correo
    if (user) { // Inicio de la validación y crfeación del email
        let i = 1; // Variable del identificador
        let userFind = {}; // Variable pra buscar usuarios dentro del whil
        // Iniciamos el While y se detendrá cuando no exista usuarios con el email creado
        while (userFind) {
            // Creamos el usuario con el identificador en la poscion actual
            newEmail = `${firstName.toLowerCase()}.${paternalSurname.toLowerCase().replaceAll(' ', '')}.${i}@cidenet.com`;
            // Buscamos si existe un usuario con el email nuevo
            userFind = await User.findOne({ email: newEmail });
            // Aumentamos el identificador en 1
            i++;
        }
    } // Fin de la validación del email
    // Retornamos el nuevo email
    return newEmail;
}



module.exports = userController;