const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    other_name: {
        type: String
    },
    paternal_surname: {
        type: String,
        required: true
    },
    maternal_surname: {
        type: String,
        required: true
    },
    country_work: {
        type: String,
        required: true
    },
    identification_type: {
        type: String,
        required: true
    },
    identification: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    admission_date: {
        type: Date,
        required: true
    },
    area: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
}, {
    timestamps: true
})

module.exports = model('Users', userSchema);