const { Router } = require('express');
const router = Router();
const userController = require('../controllers/users.controller');

// @route   POST api/users/create
// @desc    Register user
// @access  Public
router.post("/users/create", userController.createUser);

// @route GET api/v1/users
// @desc Obtener usuarios paginados
// @access Public
router.get('/users', userController.getUsers)

// @route PUT api/v1/users/:id
// @desc Editar un usuario
// @access Public
router.put('/users/:id', userController.updateUser);


module.exports = router;

